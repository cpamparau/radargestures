#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "WalabotAPI.h"
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <string>
#define WINVER 0x0500
#include <winsock2.h>
#include <windows.h>
#include <fstream>

// This sample code allows you to flip pages of a file (a pdf file, a presentation etc.) using a hand gesture.
// By moving your hand along the Walabots' y axis in a gentle wave fashion, a page would flip forward or backwards.
// After running the code, you need to bring into focus the file which you want to use the app with.
// In order to make the files' paged to flip, The code takes over the "PgUp" and "PgDn" keys of your keyboard. 
// If you wish, you can decide which keys should be pressed as the outcome of your hand gesture, by changing the virtual key-codes (in the function PageFlip)
// IMPORTANT NOTE: The hand should cross the origin of the walabot device during its movement.
#ifdef __LINUX__
#define CONFIG_FILE_PATH "/etc/walabotsdk.conf"
#else
#define CONFIG_FILE_PATH "C:\\Program Files\\Walabot\\WalabotSDK\\bin\\.config"
#endif

#define CHECK_WALABOT_RESULT(result, func_name)                 \
{                                                               \
    if (result != WALABOT_SUCCESS)                              \
    {                                                           \
        unsigned int extended = Walabot_GetExtendedError();     \
        const char* errorStr = Walabot_GetErrorString();        \
        std::cout << std::endl << "Error at " __FILE__ << ":"   \
                  << std::dec << __LINE__ << " - "              \
                  << func_name << " result is 0x" << std::hex   \
                  << result << std::endl;                       \
                                                                \
        std::cout << "Error string: " << errorStr << std::endl; \
                                                                \
        std::cout << "Extended error: 0x" << std::hex           \
                  << extended << std::endl << std::endl;        \
                                                                \
        std::cout << "Press enter to continue ...";             \
        std::string dummy;                                      \
        std::getline(std::cin, dummy);                          \
        return;                                                 \
    }                                                           \
}
// Global Variables
int gFlag = 0;                      // 0 when the array of ypos samples needs to be completely reset with new samples
const double gDeltaY = 0.6;         // The difference by which 2 successive ypos samples, are checked for increasing/decreasing tendency 
const int gSampleSizeWindow = 6;    // Number of ypos samples to be taken at each loop 
int gCleanSampleSize = 0;           // Number of yPos samples received after a single loop (not every trigger action is guaranteed to return a target)
int gRestTime = 300;                // Amount of milliseconds to rest after identifying a gesture and turning a page (gives time to move the hand out 
									//  of the arena to avoid an unintentional reading)
// Returns true if a there is at least one positive y position in YposSorted

WSADATA WSAData;
SOCKET server;
SOCKADDR_IN addr;

bool GotPositive(double YposSorted[])
{
	for (int k = 0; k < gCleanSampleSize; k++)
	{
		if (YposSorted[k] > 0)
		{
			return true;
		}
	}
	return false;
}
// Returns true if a there is at least one negative  y position in YposSorted
bool GotNegative(double YposSorted[])
{
	for (int k = 0; k < gCleanSampleSize; k++)
	{
		if (YposSorted[k] < 0)
		{
			return true;
		}
	}
	return false;
}
// Given the array YposArray - moves the actual target values to the front of the array
double* SortYposArray(double YposArray[])
{
	double YposSorted[gSampleSizeWindow];
	for (int k = 0; k < gSampleSizeWindow; k++)
	{
		if (YposArray[k] < 617)
		{
			YposSorted[gCleanSampleSize] = YposArray[k];
			gCleanSampleSize++;
		}
	}
	return YposSorted;
}
// Returns a score by checking the direction of the y coordinates in the array YposSorted
int GetScore(double YposSorted[])
{
	int score = 0;
	for (int k = 1; k < gCleanSampleSize; k++)
	{
		if ((YposSorted[k] - YposSorted[k - 1]) < gDeltaY)
		{
			score++;
		}
		else if ((YposSorted[k] - YposSorted[k - 1]) > gDeltaY)
		{
			score--;
		}
	}
	return score;
}
// Flips page
void PageFlip(double  YposArray[])
{
	double *YsortedPointer;
	YsortedPointer = SortYposArray(YposArray);
	//std::cout << "YsortedPointer: " << YsortedPointer << std::endl;
	double YposSorted[gSampleSizeWindow];
	for (int k = 0; k < gCleanSampleSize; k++)
	{
		YposSorted[k] = *(YsortedPointer + k);
	}
	if (GotNegative(YposSorted) == true && GotPositive(YposSorted) == true)
	{
		int Score = GetScore(YposSorted);
		std::cout << "Score: " << Score << std::endl;
		int CurrentCleanSampleSize = gCleanSampleSize;
		gCleanSampleSize = 0; // RESET VALUE
		if (Score > 0 && Score > (CurrentCleanSampleSize / 2.5))
		{
			INPUT ip;
			// Set up a generic keyboard event.
			ip.type = INPUT_KEYBOARD;
			ip.ki.wScan = 0; // hardware scan code for key
			ip.ki.time = 0;
			ip.ki.dwExtraInfo = 0;
			// Press the "PgDn" key
			ip.ki.wVk = 0x22; // virtual-key code for the "PgDn" key
			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));
			// Release the "PgDn" key
			ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
			SendInput(1, &ip, sizeof(INPUT));
			gFlag = 0;
			char buffer[30] = "dreapta";
			std::cout << buffer << std::endl;
			send(server, buffer, sizeof(buffer), 0);
			//std::cout << "Message sent!" << std::endl;
			Sleep(gRestTime);
		}
		else if (Score < 0 && abs(Score) >(CurrentCleanSampleSize / 2.5))
		{
			INPUT ip;
			// Set up a generic keyboard event.
			ip.type = INPUT_KEYBOARD;
			ip.ki.wScan = 0; // hardware scan code for key
			ip.ki.time = 0;
			ip.ki.dwExtraInfo = 0;
			// Press the "PgUp" key
			ip.ki.wVk = 0x21; // virtual-key code for the "PgUp" key
			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));
			// Release the "PgUp" key
			ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
			SendInput(1, &ip, sizeof(INPUT));
			gFlag = 0;
			char buffer[30] = "stanga";
			std::cout << buffer << std::endl;
			send(server, buffer, sizeof(buffer), 0);
			Sleep(gRestTime);
		}
		else
		{
			return;
		}
	}
	else
	{
		gCleanSampleSize = 0; // RESET VALUE
		return;
	}
}
// Get y of closest target projected on the Y-Z plane
double GetYposOfClosestTarget(SensorTarget *targets, int numTargets)
{
	// Set initial parameter values
	int ClosestTargetIndex = 0;
	double SizeOfClosestTargetYZ = sqrt(pow(targets[0].yPosCm, 2) + pow(targets[0].zPosCm, 2));
	for (int k = 1; k < numTargets; k++)
	{
		double SizeOfTargetYZ = sqrt(pow(targets[k].zPosCm, 2) + pow(targets[k].yPosCm, 2));
		if (SizeOfTargetYZ < SizeOfClosestTargetYZ)
		{
			ClosestTargetIndex = k;
			SizeOfClosestTargetYZ = SizeOfTargetYZ;
		}
	}
	return targets[ClosestTargetIndex].yPosCm;
}
// Get y coordinate of sensed target
double GetYpos(SensorTarget* targets, int numTargets)
{
	if (numTargets > 1)
	{
		return GetYposOfClosestTarget(targets, numTargets);
	}
	else if (numTargets == 1)
	{
		return targets->yPosCm;
	}
	else
	{
		return 617; // A value that is out of range at any case- marks that no actual target was detected
	}
}
void FlipPages_SampleCode()
{
	// --------------------
	// Variable definitions
	// --------------------
	WALABOT_RESULT res;
	// Walabot_GetStatus - output parameters
	APP_STATUS appStatus;
	double calibrationProcess; // Percentage of calibration completed, if status is STATUS_CALIBRATING
	SensorTarget *targets;
	int numTargets;
	double YposArray[gSampleSizeWindow];
	// ------------------------
	// Initialize configuration
	// ------------------------ 
	double rArenaMin = 10.0;
	double rArenaMax = 100.0;
	double rArenaRes = 2.0;
	double thetaArenaMin = -20.0;
	double thetaArenaMax = 20.0;
	double thetaArenaRes = 10.0;
	double phiArenaMin = -45.0;
	double phiArenaMax = 45.0;
	double phiArenaRes = 2.0;
	double threshold = 35.0;
	// Configure Walabot database install location (for windows)
	//char* s = const_cast<char*>("C:/Program Files/Walabot/WalabotSDK");
	//res = Walabot_SetSettingsFolder(s);
	//CHECK_WALABOT_RESULT(res, "Walabot_SetSettingsFolder");
	res = Walabot_Initialize(CONFIG_FILE_PATH);
	CHECK_WALABOT_RESULT(res, "Walabot_Initialize");
	//  Connect : Establish communication with Walabot.
	//  ==================================================
	res = Walabot_ConnectAny();
	CHECK_WALABOT_RESULT(res, "Walabot_ConnectAny");
	//  Configure : Set scan profile and arena
	//  =========================================
	//   Set Profile - SENSOR Profile
	//   Walabot recording mode is configured with the following attributes:
	//   -> Distance scanning through air; 
	//   -> High-resolution images, but slower capture rate;
	res = Walabot_SetProfile(PROF_SENSOR);
	CHECK_WALABOT_RESULT(res, "Walabot_SetProfile");
	// Set arena size and resolution with Polar coordinates:
	res = Walabot_SetArenaR(rArenaMin, rArenaMax, rArenaRes);
	CHECK_WALABOT_RESULT(res, "Walabot_SetArenaR");
	res = Walabot_SetArenaTheta(thetaArenaMin, thetaArenaMax, thetaArenaRes);
	CHECK_WALABOT_RESULT(res, "Walabot_SetArenaTheta");
	res = Walabot_SetArenaPhi(phiArenaMin, phiArenaMax, phiArenaRes);
	CHECK_WALABOT_RESULT(res, "Walabot_SetArenaPhi");
	// Set threshold
	res = Walabot_SetThreshold(threshold);
	CHECK_WALABOT_RESULT(res, "Walabot_SetThreshold");
	// Set Walabot filtering type
	res = Walabot_SetDynamicImageFilter(FILTER_TYPE_MTI);
	CHECK_WALABOT_RESULT(res, "Walabot_SetDynamicImageFilter");
	// Start: Start the system in preparation for scanning.
	//  =======================================================
	res = Walabot_Start();
	CHECK_WALABOT_RESULT(res, "Walabot_Start");
	// calibrates scanning to ignore or reduce the signals
	res = Walabot_StartCalibration();
	CHECK_WALABOT_RESULT(res, "Walabot_StartCalibration");
	// calibrates scanning to ignore or reduce the signals
	res = Walabot_GetStatus(&appStatus, &calibrationProcess);
	CHECK_WALABOT_RESULT(res, "Walabot_GetStatus");
	// Display message to the user:
	std::cout << "The app is up and running- to use the app, bring your file into focus" << '\n';
	// Loop 
	bool recording = true;
	while (recording)
	{
		if (gFlag == 0)
		{
			for (int k = 0; k < gSampleSizeWindow; k++)
			{
				//  Trigger: Scan(sense) according to profile and record signals to be 
				//  available for processing and retrieval.
				//  ====================================================================
				res = Walabot_Trigger();
				
				CHECK_WALABOT_RESULT(res, "Walabot_Trigger");
				//   Get action : retrieve the last completed triggered recording 

				//  ================================================================
				res = Walabot_GetSensorTargets(&targets, &numTargets);
				std::cout << "Targets: " << numTargets << std::endl;
				CHECK_WALABOT_RESULT(res, "Walabot_GetSensorTargets");
				YposArray[k] = GetYpos(targets, numTargets);
			}
			gFlag = 1;
			PageFlip(YposArray);
		}
		if (gFlag == 1)
		{
			for (int k = 1; k < gSampleSizeWindow; k++)
			{
				YposArray[k - 1] = YposArray[k];
			}
			res = Walabot_Trigger();
			CHECK_WALABOT_RESULT(res, "Walabot_Trigger");
			res = Walabot_GetSensorTargets(&targets, &numTargets);
			std::cout << "Targets: " << numTargets << std::endl;
			CHECK_WALABOT_RESULT(res, "Walabot_GetSensorTargets");
			YposArray[gSampleSizeWindow - 1] = GetYpos(targets, numTargets);
			PageFlip(YposArray);
		}
	}
	//  Stop and Disconnect.
	//  ======================
	res = Walabot_Stop();
	CHECK_WALABOT_RESULT(res, "Walabot_Stop");
	res = Walabot_Disconnect();
	CHECK_WALABOT_RESULT(res, "Walabot_Disconnect");
}
#ifndef _SAMPLE_CODE_
#pragma comment(lib, "Ws2_32.lib")
int main()
{
	WSAStartup(MAKEWORD(2, 0), &WSAData);
	server = socket(AF_INET, SOCK_STREAM, 0);

	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // replace the ip with your futur server ip address. If server AND client are running on the same computer, you can use the local ip 127.0.0.1
	addr.sin_family = AF_INET;
	addr.sin_port = htons(5555);
	while (connect(server, (SOCKADDR*)&addr, sizeof(addr)) == SOCKET_ERROR)
	{
		std::cout << "Connection failed!" << std::endl;
		//return 0;
	}
	std::cout << "Connected to server!" << std::endl;

	FlipPages_SampleCode();

	closesocket(server);
	WSACleanup();
	std::cout << "Socket closed." << std::endl << std::endl;
}
#endif